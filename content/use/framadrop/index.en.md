---
title: Framadrop
icon: icon.png
replaces:
    - wetransfer
---

**Framadrop** is a privacy-friendly and libre photo sharing service, run by French privacy campaigners [Framasoft][framasoft].

If you enjoy using the service [you can donate to Framasoft][frama-donate], but that’s entirely optional.

{{< infobox >}}
- **Website:** 
    - [framadrop.org](https://framadrop.org/)
{{< /infobox >}}

[framasoft]: https://framasoft.org/
[frama-donate]: https://soutenir.framasoft.org/en/