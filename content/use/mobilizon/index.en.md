---
title: Mobilizon
icon: icon.svg
lists:
    - wip
---

**Mobilizon** is a crowdfunded effort to create a [federated][fediverse] and [libre][floss] event platform. It will be a replacement to MeetUp and Facebook events.

{{< infobox >}}
- **Website:**
    - [JoinMobilizon.org](https://joinmobilizon.org/en/)
    - [Source](https://framagit.org/framasoft/mobilizon/)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}
[fediverse]: {{< relref "/articles/federated-sites" >}}