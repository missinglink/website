---
title: Whatsapp
subtitle: Instant Messengers
provider: facebook
order:
    - signal
    - xmpp
    - quicksy
    - zom
    - matrix
aliases:
    - /ethical-alternatives-to-whatsapp-and-skype/
---